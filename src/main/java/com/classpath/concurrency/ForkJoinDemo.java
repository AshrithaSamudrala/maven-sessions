package com.classpath.concurrency;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;

class UpperCasePrintTask  extends RecursiveAction {

    private final String data;

    private static  final int LIMIT = 1;

    public UpperCasePrintTask(String data){
        this.data = data;
    }
    @Override
    protected void compute() {
        if (data.length() > LIMIT){
            final Collection<UpperCasePrintTask> tasks = splitData();
            ForkJoinTask.invokeAll(tasks);
        }else {
            printData(data);
        }
    }

    private void printData(String value) {
        String result = value.toUpperCase();
        System.out.println(result);
    }

    private List<UpperCasePrintTask> splitData() {
        String leftPart = data.substring(0, data.length() / 2);
        String rightPart = data.substring(data.length() / 2);

        return Arrays.asList(new UpperCasePrintTask(leftPart), new UpperCasePrintTask(rightPart));
    }

}

public class ForkJoinDemo {
    public static void main(String[] args) {
        ForkJoinPool pool = ForkJoinPool.commonPool();
        pool.invoke(new UpperCasePrintTask("testdatatobecapitalized"));
    }
}