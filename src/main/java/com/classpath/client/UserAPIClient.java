package com.classpath.client;

import com.classpath.model.User;
import com.classpath.service.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class UserAPIClient {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
        final UserService userService = applicationContext.getBean("userService", UserService.class);

        userService.saveUser(User.builder().username("praveen").emailAddress("praveen@gmail.com").build());
        userService.saveUser(User.builder().username("vinay").emailAddress("vinay@gmail.com").build());
        userService.saveUser(User.builder().username("harish").emailAddress("harish@gmail.com").build());
        userService.saveUser(User.builder().username("vinod").emailAddress("vinod@gmail.com").build());
        userService.saveUser(User.builder().username("jagdeesh").emailAddress("jagdeesh@gmail.com").build());

        //fetch all users
        userService.fetchAllUsers().forEach(System.out::println);
        //fetch user by id
        User user = userService.fetchAllUsers().stream().findFirst().get();
        User fetchedUser = userService.fetchUserByUserId(user.getUserId());
        System.out.println("Fetched User :: "+ fetchedUser);
        System.out.println("Updated user with "+ user.getUserId() + " before updating");
       // userService.updateUser(user.getUserId(), User.builder().username("hari").emailAddress("hari@gmail.com").build());
        System.out.println("Updated user with "+ user.getUserId() + " after updating "+ user);

        System.out.println("Number of Users before deleting :: "+ userService.fetchAllUsers().size());
        //userService.deleteUserById(user.getUserId());
        System.out.println("Number of Users after deleting :: "+ userService.fetchAllUsers().size());
    }
}