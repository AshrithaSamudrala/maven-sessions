package com.classpath.client;

import com.classpath.model.Customer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;

public class UberClient {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
        //Customer ravi = new Customer(new Driver());
        Customer customer = applicationContext.getBean("customer", Customer.class);
        Customer customer2 = applicationContext.getBean("customer", Customer.class);

        System.out.println(" Are the two customers equal by reference : " + (customer == customer2));
        /*customer.bookCabAndCommute("RR-Nagar", "Airport");
        System.out.println(customer.getAddress());*/
        /*Address address = applicationContext.getBean("address", Address.class);
        System.out.println(address);*/

        String [] allBeans = applicationContext.getBeanDefinitionNames();
        Arrays.asList(allBeans).forEach(System.out::println);
        ((AbstractApplicationContext)applicationContext).registerShutdownHook();
    }
}