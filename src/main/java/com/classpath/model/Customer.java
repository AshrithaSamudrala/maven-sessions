package com.classpath.model;

import com.classpath.model.Address;
import com.classpath.model.UberGo;

public class Customer {

    private UberGo uberGo;

    private String firstName;

    private String lastName;

    private Address address;

    public Customer(UberGo uberGo, String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.uberGo = uberGo;
        System.out.println("Inside the constructor :: ");
    }

    public void cutomInitialization(){
        System.out.println("Some custom initialization logic  to be performed ::");
    }

    public void setAddress(Address address){
        System.out.println("INside the setter method :: ");
        this.address = address;
    }

    public Address getAddress(){
        return this.address;
    }

    public void bookCabAndCommute(String from, String destination){
        double bill = this.uberGo.commute(from, destination);
        System.out.println(" Reached ariport :: Total bill is :: "+ bill);
    }

    public void customCleanup(){
        System.out.println("Inside the clean up method");
    }
}